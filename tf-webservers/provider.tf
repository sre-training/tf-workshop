############################################# TERRAFORM #############################################

terraform {
  backend "s3" {
    bucket  = "tf-workshop-777788889999444433338888"
    key     = "tf-workshop.tfstate"
    region  = "us-east-1"
    profile = "tf-workshop"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.5.0"
    }
  }
}

provider "aws" {
  region  = var.region
  profile = var.profile
}

resource "random_string" "suffix" {
  length  = 7
  lower   = true
  upper   = false
  special = false
}

############################################# TERRAFORM #############################################